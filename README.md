# README #

Velocity is a collection of MAKO templates that can be included in Verilog source files to aid in faster, safer, and more scalable design practices.

### What is this repository for? ###

`vel-src/` includes the MAKO templates that Velocity comprises.
`examples/` includes sample code making use of the Velocity templates.

### Dependencies ###

Velocity is designed to produce output code that is Verilog-2001 compliant; therefore, you need a simulator or synthesis tool that can interpret, compile, etc. Verilog-2001 code.  

All of the example code is written to simulate in Icarus Verilog.