`ifndef __fsm_sv
`define __fsm_sv 1 

<%namespace file="vel-fsm.mh" import="*"/>

module fsm (
    input  logic reset,
    input  logic clk,
    input  logic F,
    input  logic I,
    input  logic Z,
    output logic B,
    output logic A,
    output logic N,
    output logic G
);

${make_fsm({
    "IDLE"   : [(" F & ~I & ~Z", "PDG_I" )],
    "PDG_I"  : [(" F & ~I & ~Z", "PDG_I" ) ,
                ("~F &  I & ~Z", "PDG_Z" ) ,
                ("~F & ~I &  Z", "IDLE"  )],
    "PDG_Z"  : [(" F & ~I & ~Z", "PDG_I" ) ,
                ("~F &  I & ~Z", "IDLE"  ) ,
                ("~F & ~I &  Z", "PDG_Z2")],
    "PDG_Z2" : [(" F & ~I & ~Z", "PDG_I" ) ,
                ("~F &  I & ~Z", "IDLE"  ) ,
                ("~F & ~I &  Z", "OUT_B" )],


    "OUT_B"  : [(" 1'b1       ", "OUT_A")],
    "OUT_A"  : [(" 1'b1       ", "OUT_N")],
    "OUT_N"  : [(" 1'b1       ", "OUT_G")],
    "OUT_G"  : [(" 1'b1       ", "IDLE" )],
})}

assign B = State.OUT_B;
assign A = State.OUT_A;
assign N = State.OUT_N;
assign G = State.OUT_G;

endmodule

`endif // __fsm_sv  
