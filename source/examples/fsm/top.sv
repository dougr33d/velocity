`include "fsm.sv"

module top();

    //
    // Nets
    //

logic clk;
logic reset;

logic F, I, Z, B, A, N, G;

    //
    // DUT
    //

fsm fsm(
    .clk, .reset, 
    .F, .I, .Z,
    .B, .A, .N, .G
);

    //
    // Stimulus
    //

initial clk = 1'b0;
always 
    clk = #100 ~clk;

`define ZERO_ALL F = 0; I = 0; Z = 0;
`define DO_F F = 1; @(posedge clk); `ZERO_ALL
`define DO_I I = 1; @(posedge clk); `ZERO_ALL
`define DO_Z Z = 1; @(posedge clk); `ZERO_ALL

initial begin
    $dumpfile("dump.vcd");
    $dumpvars(0,top);

    reset = 1'b1;
    `ZERO_ALL;
    repeat(10) @(posedge clk);
    reset = 1'b0;

    `DO_F;
    `DO_I;
    `DO_Z;
    `DO_Z;
    repeat(20) @(posedge clk);

    `DO_F;
    `DO_I;
    `DO_Z;
    `DO_Z;
    repeat(20) @(posedge clk);

    $finish;

end


    //
    // Displays
    //

    always @(negedge clk) begin
        if (F) $display("@%-5t ~ F", $time);
        if (I) $display("@%-5t ~ I", $time);
        if (Z) $display("@%-5t ~ Z", $time);
        if (B) $display("@%-5t >>>> B", $time);
        if (A) $display("@%-5t >>>> A", $time);
        if (N) $display("@%-5t >>>> N", $time);
        if (G) $display("@%-5t >>>> G", $time);
    end

endmodule
