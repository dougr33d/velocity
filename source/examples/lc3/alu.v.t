`ifndef __alu_sv
`define __alu_sv 1 

<%namespace file="vel-fsm.mh" import="*"/>

module alu (
    output wire[15:0] K,
    output wire[2:0]  NZP,
    input  wire[15:0] A,
    input  wire[15:0] B,
    input  wire[1:0]  ALUK
);

assign K = ((ALUK == 2'd0) ? A + B: 0)
         | ((ALUK == 2'd1) ? A & B: 0)
         | ((ALUK == 2'd2) ? ~A   : 0)
         | ((ALUK == 2'd3) ? A    : 0);

assign N = K[15];
assign Z = ~|K;
assign P = ~N;
assign NZP = {N,Z,P};

endmodule

`endif // __alu_sv  
