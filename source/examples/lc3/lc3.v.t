`ifndef __lc3_sv
`define __lc3_sv 1 

`include "lc3_ctl.v"
`include "reg_file.v"
`include "alu.v"
`include "memctl.v"

<%namespace file="vel-fsm.mh" import="*"/>

module lc3 (
    input  wire       reset,
    input  wire       clk
);

    //
    // Nets
    //

wire[15:0] BUS;

wire[15:0] MARMUX_OUT;
wire[15:0] PC;
wire[15:0] MDR;

wire       R;
wire[2:0]  NZP;
wire[15:0] IR;
wire       INT;

wire       LD_MAR;
wire       LD_MDR;
wire       LD_IR;
wire       LD_BEN;
wire       LD_REG;
wire       LD_CC;
wire       LD_PC;
wire       LD_Priv;
wire       LD_SavedSSP;
wire       LD_SavedUSP;
wire       LD_Vector;

wire       GatePC;
wire       GateMDR;
wire       GateALU;
wire       GateMARMUX;
wire       GateVector;
wire       GatePCm1;
wire       GatePSR;
wire       GateSP;

wire[1:0]  PCMUX;
wire[1:0]  DRMUX;
wire[1:0]  SR1MUX;
wire       ADDR1MUX;
wire[1:0]  ADDR2MUX;
wire[1:0]  SPMUX;
wire       MARMUX;
wire[1:0]  VectorMUX;
wire       PSRMUX;
wire[1:0]  ALUK;
wire       MIO_EN;
wire       R_W;
wire       Set_Priv;

wire[2:0]  DR;
wire[2:0]  SR1;
wire[2:0]  SR2;
wire       SR2MUX;
wire[2:0]  SR1MUX_OUT;
wire[2:0]  DRMUX_OUT;

wire[15:0] SR1_OUT;
wire[15:0] SR2_OUT;

wire[15:0] ALU_OUT;
wire[15:0] SR2MUX_OUT;
wire[15:0] ADDR2MUX_OUT;
wire[15:0] ADDR1MUX_OUT;

wire [2:0] ALU_NZP;
wire       BEN;


    //
    // DUT
    //

lc3_ctl lc3_ctl (
    .reset,
    .clk,

    .R,
    .BEN,
    .NZP,
    .IR,
    .INT,

    .LD_MAR,
    .LD_MDR,
    .LD_IR,
    .LD_BEN,
    .LD_REG,
    .LD_CC,
    .LD_PC,
    .LD_Priv,
    .LD_SavedSSP,
    .LD_SavedUSP,
    .LD_Vector,

    .GatePC,
    .GateMDR,
    .GateALU,
    .GateMARMUX,
    .GateVector,
    .GatePCm1,
    .GatePSR,
    .GateSP,

    .PCMUX,
    .DRMUX,
    .SR1MUX,
    .ADDR1MUX,
    .ADDR2MUX,
    .SPMUX,
    .MARMUX,
    .VectorMUX,
    .PSRMUX,
    .ALUK,
    .MIO_EN,
    .R_W,
    .Set_Priv
);

reg_file rf (
    .clk,
    .SR1,
    .SR1_OUT,
    .SR2,
    .SR2_OUT,
    .LD_REG,
    .DR,
    .DR_IN ( BUS )
);

alu alu (
    .K   ( ALU_OUT    ) ,
    .NZP ( ALU_NZP    ) ,
    .A   ( SR1_OUT    ) ,
    .B   ( SR2MUX_OUT ) ,
    .ALUK
);

    //
    // Muxes
    //

assign SR2MUX_OUT   = SR2MUX ? {{11{IR[4]}}, IR[4:0]} : SR2_OUT;
assign ADDR2MUX_OUT = ( (ADDR2MUX == 2'd0) ?  0                       :  0 )
                    | ( (ADDR2MUX == 2'd1) ? {{10{IR[ 5]}}, IR[ 5:0]} :  0 )
                    | ( (ADDR2MUX == 2'd2) ? {{ 7{IR[ 8]}}, IR[ 8:0]} :  0 )
                    | ( (ADDR2MUX == 2'd3) ? {{ 5{IR[10]}}, IR[10:0]} :  0 );
assign ADDR1MUX_OUT = ADDR1MUX ? SR1_OUT : PC;
assign MARMUX_OUT   = MARMUX ? {ADDR1MUX_OUT[15:0] + ADDR2MUX_OUT[15:0]} : {8'd0, IR[7:0]} ;
assign SR1MUX_OUT   = ( (SR1MUX == 2'd0) ? IR[11:9] : 0 )
                    | ( (SR1MUX == 2'd1) ? IR[ 8:6] : 0 )
                    | ( (SR1MUX == 2'd2) ? 3'd6     : 0 );
assign DRMUX_OUT    = ( (DRMUX  == 2'd0) ? IR[11:9] : 0 )
                    | ( (DRMUX  == 2'd1) ? 3'd7     : 0 )
                    | ( (DRMUX  == 2'd2) ? 3'd6     : 0 );

    //
    // Derived
    // 

assign SR2MUX = IR[5];
assign SR1    = SR1MUX_OUT;
assign SR2    = IR[2:0];
assign DR     = DRMUX_OUT; //IR[11:9];


    //
    // IR
    //

dff_en #(16) rgIR ( IR, BUS, clk, LD_IR );

    //
    // PC
    //

wire[15:0] PCMUX_OUT;

assign PCMUX_OUT = ( (PCMUX == 2'd0) ? PC + 16'd1                  : 0 )
                 | ( (PCMUX == 2'd1) ? BUS                         : 0 )
                 | ( (PCMUX == 2'd2) ? ADDR2MUX_OUT + ADDR1MUX_OUT : 0 );

dff_en #(16) rgPC ( PC, reset ? 16'd0 : PCMUX_OUT, clk, LD_PC );

    //
    // Memory Controller
    //

memctl memctl (
    .clk,
    .reset,
    .BUS,
    .LD_MDR,
    .LD_MAR,
    .MIO_EN,
    .R_W,
    .R,
    .MDR
);

    //
    // Interrupts
    //

assign INT = 1'b0;

    //
    // NZP
    //


assign N = BUS[15];
assign Z = ~|BUS;
assign P = ~N;
wire[2:0] NZPIn = {N,Z,P};
dff_en #(3) rgNZP ( NZP, NZPIn, clk, LD_CC );

    //
    // BEN
    //

dff_en rgBEN ( BEN, |(IR[11:9] & NZP), clk, LD_BEN );

    //
    // BUS Control
    //

assign BUS = ( GateMARMUX ? MARMUX_OUT : 0 )
           | ( GatePC     ? PC         : 0 )
           | ( GateALU    ? ALU_OUT    : 0 )
           | ( GateMDR    ? MDR        : 0 );

endmodule

`endif // __lc3_sv  
