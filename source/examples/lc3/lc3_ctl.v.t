`ifndef __lc3_ctl_sv
`define __lc3_ctl_sv 1 

<%namespace file="vel-fsm.mh" import="*"/>
<%namespace file="vel-pla.mh" import="*"/>

module lc3_ctl (
    input  wire       reset,
    input  wire       clk,
                       
    input  wire       R,
    input  wire[2:0]  NZP,
    input  wire[15:0] IR,
    input  wire       INT,
    input  wire       BEN,

    output wire       LD_MAR,
    output wire       LD_MDR,
    output wire       LD_IR,
    output wire       LD_BEN,
    output wire       LD_REG,
    output wire       LD_CC,
    output wire       LD_PC,
    output wire       LD_Priv,
    output wire       LD_SavedSSP,
    output wire       LD_SavedUSP,
    output wire       LD_Vector,

    output wire       GatePC,
    output wire       GateMDR,
    output wire       GateALU,
    output wire       GateMARMUX,
    output wire       GateVector,
    output wire       GatePCm1,
    output wire       GatePSR,
    output wire       GateSP,

    output wire[1:0]  PCMUX,
    output wire[1:0]  DRMUX,
    output wire[1:0]  SR1MUX,
    output wire       ADDR1MUX,
    output wire[1:0]  ADDR2MUX,
    output wire[1:0]  SPMUX,
    output wire       MARMUX,
    output wire[1:0]  VectorMUX,
    output wire       PSRMUX,
    output wire[1:0]  ALUK,
    output wire       MIO_EN,
    output wire       R_W,
    output wire       Set_Priv
);

    //
    // Internal defs
    //

`define INST_RTI   8
`define INST_ADD   1
`define INST_AND   5
`define INST_NOT   9
`define INST_TRAP  15
`define INST_LEA   14
`define INST_LD    2
`define INST_LDR   6
`define INST_LDI   10
`define INST_STI   11
`define INST_STR   7
`define INST_ST    3
`define INST_JSR   4
`define INST_JMP   12
`define INST_BR    0
`define INST_1101  13

    //
    // Nets
    //

wire[3:0] IR_Decode = IR[15:12];

    //
    // FSM
    //

${make_fsm({
    "S_18"   : [(" INT                     ", "S_49"   ) ,
                ("~INT                     ", "S_33"   )],
                                                      
    "S_33"   : [(" R                       ", "S_35"   )],
    "S_35"   : [(" 1'b1                    ", "S_32"   )],
                                                      
    "S_32"   : [(" IR_Decode == `INST_RTI   ", "S_08"   ),
                (" IR_Decode == `INST_ADD   ", "S_01"   ),
                (" IR_Decode == `INST_AND   ", "S_05"   ),
                (" IR_Decode == `INST_NOT   ", "S_09"   ),
                (" IR_Decode == `INST_TRAP  ", "S_15"   ),
                (" IR_Decode == `INST_LEA   ", "S_14"   ),
                (" IR_Decode == `INST_LD    ", "S_02"   ),
                (" IR_Decode == `INST_LDR   ", "S_06"   ),
                (" IR_Decode == `INST_LDI   ", "S_10"   ),
                (" IR_Decode == `INST_STI   ", "S_11"   ),
                (" IR_Decode == `INST_STR   ", "S_07"   ),
                (" IR_Decode == `INST_ST    ", "S_03"   ),
                (" IR_Decode == `INST_JSR   ", "S_04"   ),
                (" IR_Decode == `INST_JMP   ", "S_12"   ),
                (" IR_Decode == `INST_BR    ", "S_00"   ),
                (" IR_Decode == `INST_1101  ", "S_13"  )],
 
    "S_08"   : [(" 1'b1                    ", "TBD"   )], ### RTI 
    "S_01"   : [(" 1'b1                    ", "S_18"  )], ### ADD 
    "S_05"   : [(" 1'b1                    ", "S_18"  )], ### AND 
    "S_09"   : [(" 1'b1                    ", "S_18"  )], ### NOT 
    "S_14"   : [(" 1'b1                    ", "S_18"  )], ### LEA 

    "S_10"   : [(" 1'b1                    ", "S_24"  )], ### LDI 
    "S_24"   : [(" R                       ", "S_26"  )], ### LDI
    "S_26"   : [(" 1'b1                    ", "S_25"  )], ### LDI
    "S_02"   : [(" 1'b1                    ", "S_25"  )], ### LD  
    "S_06"   : [(" 1'b1                    ", "S_25"  )], ### LDR 
    "S_25"   : [(" R                       ", "S_27"  )], ### LD (shared)
    "S_27"   : [(" 1'b1                    ", "S_18"  )], ### LD (shared)

    "S_11"   : [(" 1'b1                    ", "S_29"  )], ### STI 
    "S_29"   : [(" R                       ", "S_31"  )], ### STI
    "S_31"   : [(" 1'b1                    ", "S_23"  )], ### STI
    "S_03"   : [(" 1'b1                    ", "S_23"  )], ### ST  
    "S_07"   : [(" 1'b1                    ", "S_23"  )], ### STR 
    "S_23"   : [(" 1'b1                    ", "S_16"  )], ### ST (shared)
    "S_16"   : [(" R                       ", "S_18"  )], ### ST (shared)

    "S_04"   : [(" IR[11]                  ", "S_21"  ) , ### JSR 
                ("~IR[11]                  ", "S_20"  )], ### JSR 
    "S_21"   : [(" 1'b1                    ", "S_18"  )], ### JSR 
    "S_20"   : [(" 1'b1                    ", "S_18"  )], ### JSR 

    "S_12"   : [(" 1'b1                    ", "S_18"  )], ### JMP 

    "S_00"   : [(" BEN                     ", "S_22"  ) , ### BR  
                ("~BEN                     ", "S_18"  )], ### BR  
    "S_22"   : [(" 1'b1                    ", "S_18"  )], ### BR  

    "S_13"   : [(" 1'b1                    ", "TBD"   )], ### 1101
    "S_15"   : [(" 1'b1                    ", "TBD"   )], ### TRAP
    "S_49"   : [(" 1'b1                    ", "TBD"   )], ### INT
    "TBD"    : [(" 1'b1                    ", "TBD"   )], ### Unimplemented
}, reset_state="S_18")}


${make_pla([
    ("State.S_18", "GatePC        LD_MAR                                  LD_PC                                   PCMUX<00>                                                                   ") , # FETCH
    ("State.S_33", "GateMDR              LD_MDR                                                 MIO_EN                                                                                        ") , # FETCH
    ("State.S_35", "GateMDR                     LD_IR                                                                                                                                         ") , # FETCH
    ("State.S_32", "                                  LD_BEN                                                                                                                                  ") , # DECODE

    ("State.S_01", "GateALU                                  LD_REG LD_CC              ALUK<00>                                       SR1MUX<01>                                              ") , # ADD
    ("State.S_05", "GateALU                                  LD_REG LD_CC              ALUK<01>                                       SR1MUX<01>                                              ") , # AND
    ("State.S_09", "GateALU                                  LD_REG LD_CC              ALUK<10>                                       SR1MUX<01>                                              ") , # NOT
    ("State.S_14", "GatePC                                   LD_REG                    ALUK<10>                   PCMUX<10>           SR1MUX<01> ADDR1MUX<0> ADDR2MUX<10>                     ") , # LEA
                                                                                                                                                                                    
    ("State.S_02", "GateMARMUX    LD_MAR                                                                                                         ADDR1MUX<0> ADDR2MUX<10> MARMUX<1>           ") , # LD
    ("State.S_06", "GateMARMUX    LD_MAR                                                                                              SR1MUX<01> ADDR1MUX<1> ADDR2MUX<01> MARMUX<1>           ") , # LDR
    ("State.S_10", "GateMARMUX    LD_MAR                                                                                                         ADDR1MUX<0> ADDR2MUX<10> MARMUX<1>           ") , # LDI
    ("State.S_24", "                     LD_MDR                                                 MIO_EN    R_W<0>                                                                              ") , # LDI
    ("State.S_26", "GateMDR       LD_MAR                                                                                                                                                      ") , # LDI
    ("State.S_25", "                     LD_MDR                                                 MIO_EN    R_W<0>                                                                              ") , # LD (shared)
    ("State.S_27", "GateMDR                                  LD_REG LD_CC                                                                                                                     ") , # LD (shared)
                                                                                                          
    ("State.S_03", "GateMARMUX    LD_MAR                                                                                                         ADDR1MUX<0> ADDR2MUX<10> MARMUX<1>           ") , # ST

    ("State.S_07", "GateMARMUX    LD_MAR                                                                                              SR1MUX<01> ADDR1MUX<1> ADDR2MUX<01> MARMUX<1>           ") , # STR

    ("State.S_11", "GateMARMUX    LD_MAR                                                                                                         ADDR1MUX<0> ADDR2MUX<10> MARMUX<1>           ") , # STI
    ("State.S_29", "                     LD_MDR                                                 MIO_EN    R_W<0>                                                                              ") , # STI
    ("State.S_31", "GateMDR       LD_MAR                                                                                                                                                      ") , # STI
                                                                                                          
    ("State.S_23", "GateALU              LD_MDR                                        ALUK<11> MIO_EN<0>                             SR1MUX<00>                                              ") , # ST (shared)
    ("State.S_16", "                                                                            MIO_EN<1> R_W<1>                                                                              ") , # ST (shared)
                                  
    ("State.S_04", "GatePC                                   LD_REG                                                                                                                 DRMUX<01> ") , # JSR
    ("State.S_20", "                                                      LD_PC                                   PCMUX<10>           SR1MUX<01> ADDR1MUX<1> ADDR2MUX<00>                     ") , # JSR
    ("State.S_21", "                                                      LD_PC                                   PCMUX<10>                      ADDR1MUX<0> ADDR2MUX<11>                     ") , # JSR
                                  
    ("State.S_12", "                                                      LD_PC                                   PCMUX<10>           SR1MUX<01> ADDR1MUX<1> ADDR2MUX<00>                     ") , # JSR
                                  
    ("State.S_00", "                                                                                                                                                                          ") , # BR
    ("State.S_22", "                                                      LD_PC                                   PCMUX<10>                      ADDR1MUX<0> ADDR2MUX<10>                     ") , # BR
])}

assign GateVector  = 0;
assign GatePCm1    = 0;
assign GatePSR     = 0;
assign GateSP      = 0;

// Debug

always @(negedge clk & ~reset) begin
    if          (State.S_18) begin
        $display("------------- [@%10t] -------------", $time());
        $display("FETCHING: %04x", lc3.PC);
    end

    if (State.S_08) $display("OPCODE: RTI");
    if (State.S_01) $display("OPCODE: ADD");
    if (State.S_05) $display("OPCODE: AND");
    if (State.S_09) $display("OPCODE: NOT");
    if (State.S_14) $display("OPCODE: LEA");
    if (State.S_10) $display("OPCODE: LDI");
    if (State.S_24) $display("OPCODE: LDI");
    if (State.S_26) $display("OPCODE: LDI");
    if (State.S_02) $display("OPCODE: LD");
    if (State.S_06) $display("OPCODE: LDR");
    if (State.S_25) $display("OPCODE: LD (shared)");
    if (State.S_27) $display("OPCODE: LD (shared)");
    if (State.S_11) $display("OPCODE: STI");
    if (State.S_29) $display("OPCODE: STI");
    if (State.S_31) $display("OPCODE: STI");
    if (State.S_03) $display("OPCODE: ST");
    if (State.S_07) $display("OPCODE: STR");
    if (State.S_23) $display("OPCODE: ST (shared)");
    if (State.S_16) $display("OPCODE: ST (shared)");
    if (State.S_04) $display("OPCODE: JSR");
    if (State.S_21) $display("OPCODE: JSR");
    if (State.S_20) $display("OPCODE: JSR");
    if (State.S_12) $display("OPCODE: JMP");
    if (State.S_00) $display("OPCODE: BR");
    if (State.S_22) $display("OPCODE: BR");
    if (State.S_15) begin
        $display("OPCODE: TRAP");
        $finish();
    end
end

endmodule

`endif // __lc3_ctl_sv  
