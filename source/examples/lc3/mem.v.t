`ifndef __mem_sv
`define __mem_sv 1 

<%namespace file="vel-fsm.mh" import="*"/>

module mem (
    input  wire       clk,
           
    input  wire[15:0] ADDR,
    input  wire[15:0] DIN,
    input  wire       R_W,
    input  wire       MEM_EN,

    output wire       R,
    output wire[15:0] DOUT
);

reg[15:0] RAM [0:255];


`define ADD_RRR(dr,sr1,sr2 )  (16'h1000 | ((dr) << 9) | ((sr1) << 6) | ((sr2 ) << 0))
`define ADD_RRI(dr,sr1,imm5)  (16'h1020 | ((dr) << 9) | ((sr1) << 6) | ((imm5) << 0))
`define AND_RRR(dr,sr1,sr2 )  (16'h5000 | ((dr) << 9) | ((sr1) << 6) | ((sr2 ) << 0))
`define AND_RRI(dr,sr1,imm5)  (16'h5020 | ((dr) << 9) | ((sr1) << 6) | ((imm5) << 0))
`define BR_NZP(n,z,p,pcoff9)  (16'h0000 | ((n) << 11) | ((z) << 10) | ((p) << 9) | (pcoff9))
`define JMP_R(br)             (16'hC000 | ((br) << 6))
`define JSR_I(pcoff11)        (16'h4800 | ((pcoff11)))
`define JSRR_R(br)            (16'h4000 | ((br) << 6))
`define LD_RI(dr,pcoff9)      (16'h2000 | ((dr) << 9) | (pcoff9))
`define LDI_RI(dr,pcoff9)     (16'hA000 | ((dr) << 9) | (pcoff9))
`define LDR_RRI(dr,br,off6)   (16'h6000 | ((dr) << 9) | ((br) << 6) | (off6))
`define LEA_RI(dr,pcoff9)     (16'hE000 | ((dr) << 9) | (pcoff9))
`define NOT_RR(dr,sr)         (16'h903F | ((dr) << 9) | ((sr) << 6))
`define RTI                   (16'h8000)
`define ST_RI(sr,pcoff9)      (16'h3000 | ((sr) << 9) | (pcoff9))
`define STI_RI(sr,pcoff9)     (16'hB000 | ((sr) << 9) | (pcoff9))
`define STR_RRI(sr,br,off6)   (16'h7000 | ((sr) << 9) | ((br) << 6) | (off6))
`define TRAP_I(trapvec8)      (16'hF000 | ((trapvec8)))


`define RET                  `JMP_R(7)
`define HLT                  `TRAP_I(8'hFF)
`define NOP                  `BR_NZP(0,0,0,9'h0)

reg[31:0] i;
reg[31:0] loc_loop_start;
reg[31:0] loc_loop_out;
reg[31:0] call_target = 60;
reg[31:0] call_target_delta;
always @(posedge clk) begin
    i=0                                        ; 
    RAM[i] = `AND_RRI(0,0,5'd0)              ; i=i+1;  // R0 <= 0
    RAM[i] = `ADD_RRI(0,0,5'd15)             ; i=i+1;  // R0 <= 15
    RAM[i] = `ADD_RRR(0,0,0)                 ; i=i+1;  // R0 <= 30
    RAM[i] = `ADD_RRR(0,0,0)                 ; i=i+1;  // R0 <= 60
    RAM[i] = `ADD_RRR(0,0,0)                 ; i=i+1;  // R0 <= 120

    RAM[i] = `LDR_RRI(1,0,6'd0)              ; i=i+1;  // R1 <= [120]
    RAM[i] = `LDR_RRI(2,0,6'd1)              ; i=i+1;  // R2 <= [121]

    RAM[i] = `LDR_RRI(4,0,6'd2)              ; i=i+1;  // R4 <= [122] // loop entrance
    RAM[i] = `LDR_RRI(5,0,6'd3)              ; i=i+1;  // R5 <= [123] // loop exit
    RAM[i] = `AND_RRI(3,0,5'd0)              ; i=i+1;  // R3 <= 0

    // NOW LOOP:
    loc_loop_start = i                         ; 
    RAM[i] = `ADD_RRI(1,1,5'h1f)             ; i=i+1;  // R1--
    RAM[i] = `BR_NZP(1,0,0,9'd2)             ; i=i+1;  // if (N) br out
    RAM[i] = `ADD_RRR(3,3,2)                 ; i=i+1;  // R3 =i+ R2
    RAM[i] = `JMP_R(4)                       ; i=i+1;  
    
    loc_loop_out = i                           ; 
    RAM[i] = `NOP                            ; i=i+1;  
    call_target_delta = call_target - (i + 1)  ; 
    RAM[i] = `JSR_I(call_target_delta[10:0]) ; i=i+1;  
    RAM[i] = `ADD_RRI(1,1,5'h1F)             ; i=i+1;  

    RAM[i] = `STR_RRI(7,0,6'h4)              ; i=i+1;  
    RAM[i] = `LDR_RRI(5,0,6'h4)              ; i=i+1;  
    RAM[i] = `HLT                            ; i=i+1;  


    i = call_target                            ; 
    RAM[i] = `ADD_RRI(1,1,5'd1)              ; i=i+1;  
    RAM[i] = `NOT_RR(6,7)                    ; i=i+1;  
    RAM[i] = `NOT_RR(6,6)                    ; i=i+1;  
    RAM[i] = `RET                            ; i=i+1;  


    RAM[120] = 16'd13                          ; 
    RAM[121] = 16'd5                           ; 
    RAM[122] = loc_loop_start[15:0]            ; 
    RAM[123] = loc_loop_out[15:0]              ; 
end

always @(posedge clk) begin
    if (MEM_EN & R_W) begin
        RAM[ADDR] <= DIN;
    end
end

assign R = MEM_EN;
assign DOUT = RAM[ADDR];

endmodule

`endif // __mem_sv  
