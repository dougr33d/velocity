`ifndef __memctl_sv
`define __memctl_sv 1 

<%namespace file="vel-fsm.mh" import="*"/>

`include "mem.v"

module memctl (
    input  wire       clk,
    input  wire       reset,
           
    input  wire[15:0] BUS,
    input  wire       LD_MDR,
    input  wire       LD_MAR,
    input  wire       MIO_EN,
    input  wire       R_W,

    output wire       R,
    output wire[15:0] MDR
);

    //
    // Nets
    //

wire[15:0] MAR;
wire[15:0] MDRMUX;
wire[15:0] INMUX;
wire[15:0] MEM_OUT;
wire       MEM_EN;

    //
    // Logic
    //

dff_en #(16) rgMAR ( MAR, BUS,    clk, LD_MAR );
dff_en #(16) rgMDR ( MDR, MDRMUX, clk, LD_MDR );

assign MDRMUX = MIO_EN ? INMUX : BUS;
assign INMUX  = MEM_OUT; // No KBD/Display support yet
assign MEM_EN = MIO_EN;

    // 
    // RAM
    //

mem mem (
    .clk,
    .R_W,
    .MEM_EN,
    .R,
    .ADDR ( MAR     ) ,
    .DIN  ( MDR     ) ,
    .DOUT ( MEM_OUT ) 
);

endmodule

`endif // __memctl_sv  
