`ifndef __reg_file_sv
`define __reg_file_sv 1 

<%namespace file="vel-fsm.mh" import="*"/>
<%namespace file="vel-rep.mh" import="*"/>

module reg_file (
    input  wire       clk,

    input  wire[2:0]  SR1,
    output wire[15:0] SR1_OUT,

    input  wire[2:0]  SR2,
    output wire[15:0] SR2_OUT,

    input  wire       LD_REG,

    input  wire[2:0]  DR,
    input  wire[15:0] DR_IN
);

reg[15:0] R [0:7];

assign SR1_OUT = ${or_all("(SR1 == 3'd{0}) ? R[{0}] : 0", 8)};
assign SR2_OUT = ${or_all("(SR2 == 3'd{0}) ? R[{0}] : 0", 8)};

${repeat("dff_en #(16) rg{0} ( R[{0}],  DR_IN, clk, LD_REG & (DR == {0}) );", 8)}

always @(negedge clk & ~lc3.reset) begin
    if (LD_REG) begin
        $display("R[%d] := %04x", DR, DR_IN);
    end
end

endmodule

`endif // __reg_file_sv  
