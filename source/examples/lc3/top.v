`include "lc3.v"

module top();

    //
    // Nets
    //

reg clk;
reg reset;

    //
    // DUT
    //

lc3 lc3 (
    .reset,
    .clk
);

    //
    // Stimulus
    //

initial clk = 1'b0;
always 
    clk = #100 ~clk;

initial begin
    $dumpfile("dump.vcd");
    $dumpvars(0,top);

    reset = 1'b1;
    repeat(10) @(posedge clk);
    reset = 1'b0;

    repeat(10000) @(posedge clk);

    $finish;

end

endmodule
