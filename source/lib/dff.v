module dff #(parameter integer WIDTH=1) (
    output reg [WIDTH-1:0] q,
    input  wire[WIDTH-1:0] d,
    input  wire            clk
);

always @(posedge clk) begin
    q <= d;
end

endmodule
