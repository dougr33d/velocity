module dff_en #(parameter integer WIDTH=1) (
    output logic[WIDTH-1:0] q,
    input  logic[WIDTH-1:0] d,
    input  logic            clk,
    input  logic            en
);

always @(posedge clk) begin
    q <= en ? d : q;
end

endmodule
