#!/usr/bin/python

from mako.template import Template
from mako.lookup   import TemplateLookup
from mako          import exceptions
import argparse
import os

parser = argparse.ArgumentParser()
parser.add_argument('--from-file' , required=True, type=str , help="Input MAKO file")
parser.add_argument('--to-file'   , required=True, type=str , help="Output (post-MAKO) file")
args = parser.parse_args()

dir_path = os.path.dirname(os.path.realpath(__file__))

mylookup   = TemplateLookup(directories=['{dir_path}/../vel-src/'.format(dir_path=dir_path)])

try:
    mytemplate = Template(filename=args.from_file, lookup=mylookup)
    with open(args.to_file, "w") as fh:
        fh.write(mytemplate.render())
except:
    print(exceptions.text_error_template().render())

