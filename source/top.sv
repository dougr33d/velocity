`include "includes/defines.sv"
`include "cpu/omniforth.sv"

module top();

logic clk;
logic reset;

initial begin
    reset = '1;
    clk   = 1'b0;
    $dumpfile("test.vcd");
    $dumpvars(0,top);
    #1000 reset = '0;
    #10000 $finish;
end

always begin
    #100;
    clk <= ~clk;
end

omniforth omniforth (
    .clk,
    .reset,
    .dLoadReq       (                ) ,
    .dLoadAddr      (                ) ,
    .dLoadDataValid ( '0             ) ,
    .dLoadData      ( 8'h0           ) ,
    .iLoadReq       (                ) ,
    .iLoadAddr      (                ) ,
    .iLoadDataValid ( '0             ) ,
    .iLoadData      ( 8'h0           ) 
);

endmodule
